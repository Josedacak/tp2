Aplicación web con la funcionalidad de Autenticación de usuarios y pruebas unitarias sobre la autenticación.
Se utiliza un proceso de integración continua donde se ejecutan los test y en caso de éxito se realiza el deploy.


Ambientes - Heroku

Desarrollo - https://tp2-desarrollo.herokuapp.com/app
Homologación - https://tp2-homologacion.herokuapp.com/app/
Producción - https://tp2-produccion.herokuapp.com/app/

Usuarios de prueba / password.
Desarrollo: desarrollo / 123
Homologación: homologacion / 123
Producción: produccion / 123

