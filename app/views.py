from django.http import HttpResponse
from django.template import loader
from .forms import AuthForm
from .models import Usuario
from django.shortcuts import render, redirect
def index(request):
    if request.method == 'POST':
        form = AuthForm(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['usuario']
            password = form.cleaned_data['password']
            if(Usuario.objects.filter(nombre=usuario, password=password).count()):
                return redirect('home')
            else:
                error = 'Usuario o contraseña invalidos'
                return render(request, 'index.html', {'form': form, 'error': error})
        return render(request, 'index.html', {'form': form})
    else:
        form = AuthForm()
        return render(request, 'index.html', {'form': form})

def home(request):
    return render(request, 'home.html')
