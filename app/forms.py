from django import forms

class AuthForm(forms.Form):
    usuario = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control margin-bottom'}))
    password = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'type' : 'password'}))
