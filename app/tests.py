# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from .models import Usuario
from django.test import Client
class testLogin(TestCase):
    def setUp(self):
        client = Client()
        Usuario.objects.create(nombre= 'Jose', email= 'josedacak@gmail.com', password= '123')

    def test_login(self):
        client = Client()
        response = client.get('/app/')
        self.assertEqual(response.status_code, 200)

        response = client.post('/app/', {'usuario': 'Jose', 'password': '123'})
        self.assertEqual(response.status_code, 302)

        response = client.post('/app/', {'usuario': 'Jose', 'password': '1234'})
        self.assertEqual(response.status_code, 200)

        response = client.post('/app/', {'usuario': 'Jose'})
        self.assertEqual(response.status_code, 200)
